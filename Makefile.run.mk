docker_run:
	docker run -d \
	  --rm \
	  --name=user-setup_test_run \
		-v $(PWD)/test:/data \
		-p 8000:8000 \
	  $(DOCKER_IMAGE):$(DOCKER_TAG)
	docker ps | grep user-setup_test_run

docker_stop:
	docker rm -f user-setup_test_run 2> /dev/null ; true