<!DOCTYPE html>
<html lang="en">

<head>

  <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta charset="utf-8">
  <title>Your page title here :)</title>
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- FONT
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">

  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/skeleton.css">

  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="icon" type="image/png" href="images/favicon.png">

  <script
  src="http://code.jquery.com/jquery-1.12.4.min.js"
  integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
  crossorigin="anonymous"></script>

</head>

<body>

  <!-- Primary Page Layout
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <?php if ($_GET['credentialsstored'] == 1) : ?>

    <div class="container">
      <div class="row">
        <div class="one-half column" style="margin-top: 25%">
          <h3>Your credentials have been saved</h3>
          <p>Please check below the status of the initial setup. Once it has finished, unplug and plug the Raspberry Pi in order to reboot. After a couple of minutes the first files should have been synchronized to <a href="http://homesmarthome.5square.de">homesmarthome.5square.de.</a>. Otherwise, re-enter and save your credentials and try again.</p>
        </div>
      </div>

      <div class="row">
        <div class="one-half column">
          <h4>Installation Status</h4>
          <h3 is="status"></h3>
        </div>
      </div>
    </div>

  <?php else: ?>

    <div class="container">
      <div class="row">
        <div class="one-half column" style="margin-top: 25%">
          <h4>Installation Status</h4>
          <h3 id="status"></h3>
        </div>
      </div>
    </div>

  <?php endif; ?>

  <script>
  function refresh() {
    $.get( "status.php", function( data ) {
      $("#status" ).html( data );
      setTimeout(refresh, 5000);
    });
  }

  $( document ).ready(function() {
    refresh();
  });
  </script>
  <!-- End Document
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
</body>

</html>
