# user-setup #

[![](https://images.microbadger.com/badges/version/homesmarthome/user-setup.svg)](https://microbadger.com/images/homesmarthome/user-setup "")

[![](https://images.microbadger.com/badges/image/homesmarthome/user-setup.svg)](https://hub.docker.com/r/homesmarthome/user-setup/ "")

MQTT Bridge to IKEA Tradfri Gateway to control smart bulbs.

## Install as a Service with Portainer
1. Open portainer ([homesmarthome.local:9000](http://homesmarthome.local:9000))
2. Click on *Services* in the menu
3. Click on *+ Add Service*
4. Configuration:

    * Name: `user-setup`
        * Image configuration
        * Name: ![](https://img.shields.io/badge/dynamic/json.svg?label=Image&url=https%3A%2F%2Fregistry.hub.docker.com%2Fv2%2Frepositories%2Fhomesmarthome%2Fuser-setup%2Ftags&query=%24.results%5B1%5D.name&colorB=4286f4&prefix=homesmarthome/user-setup:&style=flat-square)
    * Ports configuration
        * Port mapping (`+map additional port`)
            * host: `80`
            * container: `8000`
    * Volumes (map additinal volume)
        * container: `/data` (`Bind`)
        * host: `/home/pi` (`Read-only`)

## Docker Service creation
```
docker service create \
  --name=user-setup \
  --mount=type=bind,src=/home/pi,dst=/data \
  --publish=80:8000/tcp \
  homesmarthome/user-setup
```

## Run
```
VERSION=latest
CREDENTIALS_DIR=/home/pi

docker run -d \
    --rm \
    --name=user-setup \
    -v $CREDENTIALS_DIR:/data \
    -p 8000:8000 \
    homesmarthome/user-setup:$VERSION
```